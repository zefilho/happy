
import {Request, Response} from 'express';
import {getRepository} from 'typeorm';

import Orphanage from '../models/Orphanage';

export default {

    async show(req:Request, res:Response){

        const {id} = req.params;

        const orphanageRep = getRepository(Orphanage);

        const orphanage = await orphanageRep.findOneOrFail(id);

        return res.json(orphanage);
    },
    
    async index(req:Request, res:Response){
        const orphanageRep = getRepository(Orphanage);

        const orphanages = await orphanageRep.find();

        return res.json(orphanages);
    },

    

    async create(req:Request, res: Response){
        const {
            name,
            latitude,
            longitude,
            about,
            instructions,
            opening_hours,
            open_on_weekends
        } = req.body;
    
        console.log(req.body);
    
        const orphanageRep = getRepository(Orphanage);

        const requestImages = req.files as Express.Multer.File[];
        
        const images = requestImages.map(image => {
            return {path: image.filename}
        })
        
        const orphanage = orphanageRep.create({
            name,
            latitude,
            longitude,
            about,
            instructions,
            opening_hours,
            open_on_weekends,
            images
        });
    
        await orphanageRep.save(orphanage);
    
        return res.status(201).json(orphanage)
    }
}