import express from 'express';

import './database/connection';

import routes from './routes';

const app = express();

app.use(express.json()); //utilizando json

app.use(routes);

//Rote - Conjunto
//Recurso - usuario -
//Metodo - Http (Get, post, put, delete)


//Parametros
//Query: localhost/users?search=diego
//Routes: localhost/users/1 (identificar recurso)
//Body localhost/users 




app.listen(3333);
