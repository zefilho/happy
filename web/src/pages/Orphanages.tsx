import React from 'react';

import {Link} from 'react-router-dom';
import {FiPlus} from 'react-icons/fi';

import { Map, TileLayer } from 'react-leaflet';

import mapMarkerImg from '../images/map-marker.svg';

import '../styles/pages/orphanage.css';

import 'leaflet/dist/leaflet.css';

function Orphanage(){
    return (
        <div id="page-map">
            <aside>
                <header>
                    <img src={mapMarkerImg} alt="Happy"/>
                    <h2>Escolha um ofarnato no mapa</h2>
                    <p>Muitas crianças estão esperando sua visita.</p>
                </header>

                <footer>
                    <strong>Patos</strong>
                    <span>Paraíba</span>
                </footer>

            </aside>

            <Map 
                center={[-7.0291078,-37.2801238]}
                zoom={15}
                style={{ width: '100%', height: '100%'}}
            > 
            
            <TileLayer url="https://a.tile.openstreetmap.org/{z}/{x}/{y}.png" />

            </Map>

            <Link to="" className="create-orphanage">
                <FiPlus size={32} color="#FFF" />
            </Link>

        </div>
    );
}

export default Orphanage;